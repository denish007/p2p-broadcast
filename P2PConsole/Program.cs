﻿using Orekaria.Lib.P2P;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace P2PConsole
{
    class Program : IDisposable
    {
        private readonly BroadcastHelper _broadcast;
        private readonly Thread _th;

        private Program()
        {
            _broadcast = new BroadcastHelper(8010, "K_*?gR@Ej");
            _th = new Thread(DequeueMessages);
            _th.Start();
        }

        #region IDisposable Members

        public void Dispose()
        {
            _th.Abort();
        }

        #endregion

        private void Run()
        {
            var isExit = false;
            do
            {
                var s = Console.ReadLine();
                if (s != null && s.ToLower() == "exit")
                    isExit = true;
                else
                    _broadcast.Send(string.Format("{0}: {1}", Environment.UserName, s));
            } while (!isExit);
        }

        private void DequeueMessages()
        {
            while (_th.IsAlive)
            {
                Thread.Sleep(100);
                while (_broadcast.Received.Count > 0)
                    Console.WriteLine(string.Format("{0}", _broadcast.Received.Dequeue()));
            }
        }

        private static void Main()
        {
            var p = new Program();
            p.Run();
            p.Dispose();
        }
    }
}
