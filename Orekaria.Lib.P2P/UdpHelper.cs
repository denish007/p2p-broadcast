using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Orekaria.Lib.P2P
{
    internal class UdpHelper
    {
        private readonly UdpClient _udpClient;
        private Queue<string> _received;
        private IPEndPoint _remoteEP;

        public UdpHelper(UdpClient udpClient, IPEndPoint remoteEP) {
            _udpClient = udpClient;
            _remoteEP = remoteEP;
        }

        /// <summary>
        ///   Change to your preferred enconding
        /// </summary>
        private static Encoding ChosenEncoder {
            get { return Encoding.UTF8; }
        }

        public Queue<string> Received {
            get {
                if (_received == null) {
                    _received = new Queue<string>();
                    _udpClient.BeginReceive(ReceiveCallback, null);
                }
                return _received;
            }
        }

        ~UdpHelper() {
            _udpClient.Close();
            Debug.Print("Resources freed");
        }

        public void Send(String s) {
            var packetBytes = ChosenEncoder.GetBytes(s);
            _udpClient.Send(packetBytes, packetBytes.Length, _remoteEP);
            // _udpClient.BeginSend(packetBytes, packetBytes.Length, _remoteEP,null,null); // could receive broken packets
            Debug.Print(string.Format("Message sent from {0} on port {1}", _remoteEP.Address, _remoteEP.Port));
        }

        private void ReceiveCallback(IAsyncResult ar) {
            var receiveBytes = _udpClient.EndReceive(ar, ref _remoteEP);
            Debug.Print(string.Format("Message received from {0} on port {1}", _remoteEP.Address, _remoteEP.Port));
            _received.Enqueue(ChosenEncoder.GetString(receiveBytes));
            _udpClient.BeginReceive(ReceiveCallback, null);
        }
    }
}